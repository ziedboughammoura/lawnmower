package fr.kata.test.entities;

import fr.kata.test.enums.Orientation;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class LawnMowerPositionTest {

    @Test
    @DisplayName("vérifie que les champs sont correctement affectés")
    void souldSetAndGetFields(){
        LawnMowerPosition position = new UnderTest();
        Coordinates c0 = new Coordinates(-1,1);

        position.setLawnMowerCoordinates(c0);
        position.setLawnMowerOrientation(Orientation.NORTH);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(position.getLawnMowerCoordinates().getX()).isEqualTo(-1);
        softly.assertThat(position.getLawnMowerCoordinates().getY()).isEqualTo(1);
        softly.assertThat(position.getLawnMowerOrientation()).isEqualTo(Orientation.NORTH);
    }

    @Test
    @DisplayName("vérifie les surcharge des donnés est correct")
    public void shouldOverloadFields() {

        LawnMowerPosition positionLM = new LawnMowerPosition(new Coordinates(5, 5), Orientation.NORTH);
        LawnMowerPosition positionLMOk = new LawnMowerPosition(new Coordinates(5, 5), Orientation.NORTH);
        LawnMowerPosition positionLMKo = new LawnMowerPosition(new Coordinates(5, 5), Orientation.SOUTH);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(positionLM.equals(positionLMOk)).isTrue();
        softly.assertThat(positionLM.equals(positionLMKo)).isFalse();
    }

    public static class UnderTest extends LawnMowerPosition {

    }
}
