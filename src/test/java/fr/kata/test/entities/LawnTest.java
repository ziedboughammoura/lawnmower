package fr.kata.test.entities;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class LawnTest {

    @Test
    @DisplayName("vérifie les surcharge des donnés est correct")
    public void shouldOverloadFields() {
        Lawn l1 = new UnderTest();
        Coordinates c1 = new Coordinates(1, 2);
        l1.setPositionMax(c1);

        Lawn l2 = new UnderTest();
        Coordinates c2 = new Coordinates(1, 2);
        l2.setPositionMax(c2);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(l1.equals(l2)).isTrue();

        Coordinates c22 = new Coordinates(1, 3);
        l2.setPositionMax(c22);
        softly.assertThat(l1.equals(l2)).isFalse();
    }

    public static class UnderTest extends Lawn {

    }
}
