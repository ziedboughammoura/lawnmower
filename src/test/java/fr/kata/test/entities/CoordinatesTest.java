package fr.kata.test.entities;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CoordinatesTest {

    @Test
    @DisplayName("vérifie que les champs sont correctement affectés")
    void souldSetAndGetFields(){
        Coordinates coordinates = new CoordinatesTest.UnderTest();
        coordinates.setX(1);
        coordinates.setY(1);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(coordinates.getX()).isEqualTo(1);
        softly.assertThat(coordinates.getY()).isEqualTo(1);
    }

    @Test
    @DisplayName("vérifie les surcharge des donnés est correct")
    public void shouldOverloadFields() {
        Coordinates c1 = new Coordinates(1, 2);
        Coordinates c2 = new Coordinates(1, 2);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(c1.equals(c2)).isTrue();

        //on change la valeur de c2
        c2 = new Coordinates(1, 3);
        softly.assertThat(c1.equals(c2)).isFalse();
    }

    @Test
    @DisplayName("vérifie que la position de la tondeuse après mouvement sont en dehors de celles de la pelouse")
    public void ShouldGetTrueIfIsOutMaxCoordinates(){
        Coordinates pCoordinates = new Coordinates(5,5);
        Coordinates c1 = new Coordinates(1,1);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(pCoordinates.isOutMaxCoordinates(c1)).isTrue();
    }

    @Test
    @DisplayName("vérifie que la position de la tondeuse après mouvement ne sont pas en dehors de celles de la pelouse")
    public void ShouldGetFalseIfIsNotOutMaxCoordinates(){
        Coordinates pCoordinates = new Coordinates(5,5);
        Coordinates c0 = new Coordinates(-1,1);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(pCoordinates.isOutMaxCoordinates(c0)).isFalse();
    }

    public static class UnderTest extends Coordinates {

    }
}
