package fr.kata.test.business;


import fr.kata.test.business.impl.LawnMowerProcessServiceImpl;
import fr.kata.test.entities.Coordinates;
import fr.kata.test.entities.Lawn;
import fr.kata.test.entities.LawnMowerPosition;
import fr.kata.test.enums.Instruction;
import fr.kata.test.enums.Orientation;
import fr.kata.test.exceptions.LawnMowerException;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LawnMowerProcessServiceImplTest {

    InstrutctionProcessService instrutctionProcessService = new InstrutctionProcessServiceImplTest.underTest();
    final Coordinates coordinatesMax = new Coordinates(5, 5);

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse")
    public void ShouldlaunchInstructions() throws LawnMowerException {
        int x = 0;
        int y = 0;
        Coordinates coordonnees = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordonnees, Orientation.NORTH);

        LawnMowerProcessService process = LawnMowerProcessServiceImpl.builder()
                .position(lawnMowerPosition)
                .lawn(new Lawn(coordinatesMax))
                .instructionsList(new ArrayList<>())
                .build();

        process.launchInstructions();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(process.toString()).isEqualTo("0 0 N");
    }


    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 1")
    public void shouldExecuteInstructionsCase_1() throws LawnMowerException {
        List<Instruction> instructionList = new ArrayList<>();
        instructionList.add(Instruction.LEFT);
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.LEFT);
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.LEFT);
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.LEFT);
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.MOVE_FORWARD);
        int x = 1;
        int y = 2;
        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates, Orientation.NORTH);

        LawnMowerProcessService process =  LawnMowerProcessServiceImpl.builder()
                .position(lawnMowerPosition)
                .lawn(new Lawn(coordinatesMax))
                .instructionsList(instructionList)
                .build();

        process.launchInstructions();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(process.toString()).isEqualTo("1 3 N");
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 2")
    public void shouldExecuteInstructionsCase_2() throws LawnMowerException {
        List<Instruction> instructionList = new ArrayList<>();
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.RIGHT);
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.RIGHT);
        instructionList.add(Instruction.MOVE_FORWARD);
        instructionList.add(Instruction.RIGHT);
        instructionList.add(Instruction.RIGHT);
        instructionList.add(Instruction.MOVE_FORWARD);
        int x = 3;
        int y = 3;
        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates, Orientation.EAST);

        LawnMowerProcessService process =  LawnMowerProcessServiceImpl.builder()
                .position(lawnMowerPosition)
                .lawn(new Lawn(coordinatesMax))
                .instructionsList(instructionList)
                .build();

        process.launchInstructions();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(process.toString()).isEqualTo("5 1 E");
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 3")
    public void shouldMoveForwardNorthInstruction() throws LawnMowerException {
        int x = 0;
        int y = 0;
        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.NORTH);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.MOVE_FORWARD,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y+1));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.NORTH);
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 4")
    public void shouldMoveForwardEastInstruction() throws LawnMowerException {
        int x = 0;
        int y = 0;
        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.EAST);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.MOVE_FORWARD,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x+1, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.EAST);
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 4")
    public void shouldMoveForwardSouthInstruction() throws LawnMowerException {
        int x = 5;
        int y = 5;

        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.EAST);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.MOVE_FORWARD,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y-1));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.SOUTH);
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 5")
    public void shouldMoveForwardWestInstruction() throws LawnMowerException {
        int x = 5;
        int y = 5;
        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.EAST);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.MOVE_FORWARD,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x-1, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.WEST);
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 6")
    public void shouldRotateLeftWestInstruction() throws LawnMowerException {
        int x = 2;
        int y = 3;
        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.WEST);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.LEFT,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.SOUTH);
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 7")
    public void shouldRotateLeftEastInstruction() throws LawnMowerException {
        int x = 2;
        int y = 3;

        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.EAST);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.LEFT,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.NORTH);
    }


    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 8")
    public void shouldRotateLeftSouthInstruction() throws LawnMowerException {
        int x = 2;
        int y = 3;

        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.SOUTH);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.LEFT,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.EAST);
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 9")
    public void shouldRotateRightNorthInstruction() throws LawnMowerException {
        int x = 2;
        int y = 3;

        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.NORTH);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.RIGHT,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.EAST);
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 10")
    public void shouldRotateRightEastInstruction() throws LawnMowerException {
        int x = 2;
        int y = 3;

        Coordinates coordinates = new Coordinates(x, y);
        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates,Orientation.EAST);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.RIGHT,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.SOUTH);
    }

    @Test
    @DisplayName("vérifie l'execution des insctructions de la tondeuse - cas 11")
    public void shouldRotateRightWestInstruction() throws LawnMowerException {
        int x = 2;
        int y = 3;
        Coordinates coordinates = new Coordinates(x, y);

        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates, Orientation.WEST);

        instrutctionProcessService.executeInstruction(
                lawnMowerPosition,
                Instruction.RIGHT,
                coordinatesMax
        );

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.NORTH);
    }
}
