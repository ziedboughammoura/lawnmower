package fr.kata.test.business;

import fr.kata.test.business.impl.InstrutctionProcessServiceImpl;
import fr.kata.test.entities.Coordinates;
import fr.kata.test.entities.LawnMowerPosition;
import fr.kata.test.enums.Instruction;
import fr.kata.test.enums.Orientation;
import fr.kata.test.exceptions.LawnMowerException;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class InstrutctionProcessServiceImplTest {

    InstrutctionProcessService instrutctionProcessService = new underTest();

    @Test
    @DisplayName("Vérifie la methode executeInstruction")
    public void shouldExecuteInstructionLaunchRotateRight() throws LawnMowerException {
        int x = 2; int y = 3;

        Coordinates coordinatesMax = new Coordinates(5, 5);
        Coordinates coordinates = new Coordinates(x, y);

        LawnMowerPosition lawnMowerPosition = new LawnMowerPosition(coordinates, Orientation.SOUTH);

        instrutctionProcessService.executeInstruction(lawnMowerPosition, Instruction.RIGHT, coordinatesMax);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(lawnMowerPosition.getLawnMowerCoordinates()).isEqualTo(new Coordinates(x, y));
        softly.assertThat(lawnMowerPosition.getLawnMowerOrientation()).isEqualTo(Orientation.WEST);
    }

    @Test
    @DisplayName("Vérifie la methode rotateLeft")
    public void shouldRotateLeft() throws LawnMowerException {
        Orientation nextOrientation = instrutctionProcessService.rotateLeft(Orientation.EAST);
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(nextOrientation).isEqualTo(Orientation.NORTH);

        nextOrientation = instrutctionProcessService.rotateLeft(Orientation.WEST);
        softly.assertThat(nextOrientation).isEqualTo(Orientation.SOUTH);

        nextOrientation = instrutctionProcessService.rotateLeft(Orientation.NORTH);
        softly.assertThat(nextOrientation).isEqualTo(Orientation.WEST);

        nextOrientation = instrutctionProcessService.rotateLeft(Orientation.SOUTH);
        softly.assertThat(nextOrientation).isEqualTo(Orientation.EAST);
    }

    @Test
    @DisplayName("Vérifie la methode rotateRight")
    public void shouldRotateRight() throws LawnMowerException {

        Orientation nextOrientation = instrutctionProcessService.rotateRight(Orientation.EAST);
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(nextOrientation).isEqualTo(Orientation.SOUTH);

        nextOrientation = instrutctionProcessService.rotateRight(Orientation.WEST);
        softly.assertThat(nextOrientation).isEqualTo(Orientation.NORTH);

        nextOrientation = instrutctionProcessService.rotateRight(Orientation.NORTH);
        softly.assertThat(nextOrientation).isEqualTo(Orientation.EAST);

        nextOrientation = instrutctionProcessService.rotateRight(Orientation.SOUTH);
        softly.assertThat(nextOrientation).isEqualTo(Orientation.WEST);
    }

    static class underTest extends InstrutctionProcessServiceImpl {

    }
}
