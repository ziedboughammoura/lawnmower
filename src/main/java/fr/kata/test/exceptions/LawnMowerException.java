package fr.kata.test.exceptions;

public class LawnMowerException extends Exception {
    public LawnMowerException(String message) {
        super(message);
    }
}