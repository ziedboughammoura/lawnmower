package fr.kata.test.business;

import fr.kata.test.exceptions.LawnMowerException;

/**
 * Service LawnMowerProcessService
 *
 * @author zied.boughammoura
 */
public interface LawnMowerProcessService {

    void launchInstructions() throws LawnMowerException;
}
