package fr.kata.test.business.impl;

import fr.kata.test.business.InstrutctionProcessService;
import fr.kata.test.business.LawnMowerProcessService;
import fr.kata.test.entities.Lawn;
import fr.kata.test.entities.LawnMowerPosition;
import fr.kata.test.enums.Instruction;
import fr.kata.test.exceptions.LawnMowerException;
import lombok.*;

import java.util.List;

/**
 * Implémentation du service {@link LawnMowerProcessService}
 *
 * @author zied.boughammoura
 *
 */

@Data
@Builder
@RequiredArgsConstructor
public class LawnMowerProcessServiceImpl implements LawnMowerProcessService {

    private final Lawn lawn;

    private final LawnMowerPosition position;

    private final List<Instruction> instructionsList;

    protected final InstrutctionProcessService instrutctionProcessService = new InstrutctionProcessServiceImpl();


    /**
     * Execute les insctructions de la tondeuse
     */
    public void launchInstructions() throws LawnMowerException {
        for(Instruction instruction : instructionsList){
            this.instrutctionProcessService.executeInstruction(position,
                    instruction, this.lawn.getPositionMax());
        }
    }

    public String toString(){
        return 	position.getLawnMowerCoordinates().getX()
                + " "
                + position.getLawnMowerCoordinates().getY()
                + " "
                + position.getLawnMowerOrientation().orientationCodeValue;
    }
}
