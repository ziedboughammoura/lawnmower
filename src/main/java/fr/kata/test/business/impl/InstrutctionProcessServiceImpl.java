package fr.kata.test.business.impl;

import fr.kata.test.business.InstrutctionProcessService;
import fr.kata.test.constants.LogMessagesConstants;
import fr.kata.test.entities.Coordinates;
import fr.kata.test.entities.LawnMowerPosition;
import fr.kata.test.enums.Instruction;
import fr.kata.test.enums.Orientation;
import fr.kata.test.exceptions.LawnMowerException;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Implémentation du service {@link InstrutctionProcessService}
 *
 * @author zied.boughammoura
 */
@Data
@Builder
@RequiredArgsConstructor
public class InstrutctionProcessServiceImpl implements InstrutctionProcessService {

    /**
     * Permet  d'avancer la tondeuse
     * @param lawnMowerPosition : position initiale de la tondeuse
     * @param coordinatesMax : coordonnees de la pelouse - coin superieur droit de la pelouse
     * @return coordonnees : nouvelles coordonnees de la tondeuse
     * @throws LawnMowerException
     */
    public Coordinates moveForwardLawnMower(LawnMowerPosition lawnMowerPosition, Coordinates coordinatesMax)
            throws LawnMowerException {

        Coordinates nextCoordinates = null;
        int x, y;

        switch (lawnMowerPosition.getLawnMowerOrientation()) {
            case NORTH -> {
                x = lawnMowerPosition.getLawnMowerCoordinates().getX();
                y = lawnMowerPosition.getLawnMowerCoordinates().getY() + 1;
            }
            case EAST -> {
                x = lawnMowerPosition.getLawnMowerCoordinates().getX() + 1;
                y = lawnMowerPosition.getLawnMowerCoordinates().getY();
            }
            case SOUTH -> {
                x = lawnMowerPosition.getLawnMowerCoordinates().getX();
                y = lawnMowerPosition.getLawnMowerCoordinates().getY() - 1;
            }
            case WEST -> {
                x = lawnMowerPosition.getLawnMowerCoordinates().getX() - 1;
                y = lawnMowerPosition.getLawnMowerCoordinates().getY();
            }
            default -> throw new LawnMowerException(LogMessagesConstants.ERREUR_POSITION_INCORRECTE);
        }

        nextCoordinates = new Coordinates(x, y);

        if (coordinatesMax.isOutMaxCoordinates(nextCoordinates)) {
            return nextCoordinates;
        } else {
            return lawnMowerPosition.getLawnMowerCoordinates();
        }
    }

    /**
     * Permet de pivoter la tondeuse à droite
     * @param orientation : orientation initiale de la tondeuse
     * @return nouvelle orientation
     * @throws LawnMowerException
     */

    public Orientation rotateRight(Orientation orientation) throws LawnMowerException{
        Orientation nextOrientation = null ;
        switch (orientation) {
            case NORTH -> nextOrientation = Orientation.EAST;
            case EAST -> nextOrientation = Orientation.SOUTH;
            case SOUTH -> nextOrientation = Orientation.WEST;
            case WEST -> nextOrientation = Orientation.NORTH;
            default -> throw new LawnMowerException(LogMessagesConstants.ERREUR_ORIENTATION_INCORRECTE);
        }
        return nextOrientation;
    }

    /**
     * Permet de pivoter la tondeuse à gauche
     * @param orientation : orientation initale de la tondeuse
     * @return nouvelle orientation
     * @throws LawnMowerException
     */
    public Orientation rotateLeft(Orientation orientation) throws LawnMowerException{
        Orientation nextOrientation = null ;
        switch (orientation) {
            case NORTH -> nextOrientation = Orientation.WEST;
            case EAST -> nextOrientation = Orientation.NORTH;
            case SOUTH -> nextOrientation = Orientation.EAST;
            case WEST -> nextOrientation = Orientation.SOUTH;
            default -> throw new LawnMowerException(LogMessagesConstants.ERREUR_ORIENTATION_INCORRECTE);
        }
        return nextOrientation;
    }

    /**
     * Execute une seule instruction ( A, D ou G)
     *
     * @param lawnMowerPosition
     * @param instruction
     * @param coordinatesMax
     *
     * @throws LawnMowerException
     */
    public void executeInstruction(
            LawnMowerPosition lawnMowerPosition,
            Instruction instruction,
            Coordinates coordinatesMax) throws LawnMowerException{

        switch (instruction) {
            case MOVE_FORWARD -> lawnMowerPosition.setLawnMowerCoordinates(
                    moveForwardLawnMower(lawnMowerPosition, coordinatesMax)
            );
            case RIGHT -> lawnMowerPosition.setLawnMowerOrientation(
                    rotateRight(lawnMowerPosition.getLawnMowerOrientation())
            );
            case LEFT -> lawnMowerPosition.setLawnMowerOrientation(
                    rotateLeft(lawnMowerPosition.getLawnMowerOrientation())
            );
            default -> throw new LawnMowerException(LogMessagesConstants.ERREUR_INSTRUCTION_INCORRECTE);
        }
    }
}
