package fr.kata.test.business;

import fr.kata.test.entities.Coordinates;
import fr.kata.test.entities.LawnMowerPosition;
import fr.kata.test.enums.Instruction;
import fr.kata.test.enums.Orientation;
import fr.kata.test.exceptions.LawnMowerException;

/**
 * Service InstrutctionProcessService
 *
 * @author zied.boughammoura
 */
public interface InstrutctionProcessService {

    Coordinates moveForwardLawnMower(LawnMowerPosition lawnMowerPosition, Coordinates coordinatesMax) throws LawnMowerException;
    Orientation rotateRight(Orientation orientation) throws LawnMowerException;
    Orientation rotateLeft(Orientation orientation) throws LawnMowerException;

    void executeInstruction(
            LawnMowerPosition lawnMowerPosition,
            Instruction instruction,
            Coordinates coordinatesMax) throws LawnMowerException;
}
