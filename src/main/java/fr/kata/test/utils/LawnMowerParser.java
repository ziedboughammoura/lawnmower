package fr.kata.test.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Classe permetant de valider les informations qui permettent de lancer une tandeuse
 *
 * @author zied.boughammoura
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LawnMowerParser {

    /**
     * Pelouse
     */
    private String lawn ;

    /**
     * Tendeuse de pelouse
     */
    private String lawnMower ;

    /**
     * Instructions de la tendeuse de pelouse
     */
    private String instructions ;


    /**
     * @return true si les informations de la tondeuse sont correctes, false sinon
     */
    public boolean executeParse(){
        return DataParser.parseLawnMower(lawnMower)
                && DataParser.parseLawn(lawn)
                && DataParser.parseInstructionsList(instructions);
    }
}
