package fr.kata.test.utils;

import fr.kata.test.business.LawnMowerProcessService;
import fr.kata.test.business.impl.LawnMowerProcessServiceImpl;
import fr.kata.test.constants.LogMessagesConstants;
import fr.kata.test.exceptions.LawnMowerException;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Classe de traitements de fichier
 *
 * @author Zied Boughammoura
 */

@Slf4j
public class FileUtils {

    /**
     * lecture/validation du fichier et lancement des tondeuses
     *
     * @param file
     *
     * @throws LawnMowerException
     * @throws IOException
     *
     * @return liste de positions des tondeuses
     */
    public static List<String> launchLawnMowerProcess(File file) throws IOException, LawnMowerException {
        if (!file.isFile()) {
            throw new LawnMowerException(LogMessagesConstants.ERREUR_FICHIER_INEXISTANT);
        } else {
            LawnMowerParser parser = new LawnMowerParser();
            try (Scanner scanner = new Scanner(file)) {
                processFirstLine(parser, scanner);
                return processNextLines(parser, scanner);
            }
        }
    }

    /**
     * traite la premiere ligne du fichier
     * @param parser
     * @param scanner
     * @throws LawnMowerException : erreur si le fichier contients moins de 2 lignes
     */
    protected static void processFirstLine(LawnMowerParser parser, Scanner scanner)
            throws LawnMowerException {
        if (scanner.hasNext()) {
            parser.setLawn(scanner.nextLine());
        }
        if (!scanner.hasNext()) {
            throw new LawnMowerException(LogMessagesConstants.ERREUR_DONNEES_INCORRECTES);
        }
    }

    /**
     *
     * @param parser
     * @param scanner
     * @return la position des tondeuses
     * @throws LawnMowerException
     */
    private static List<String> processNextLines(LawnMowerParser parser,
                                                 Scanner scanner) throws LawnMowerException {
        List<String> listePositions = new ArrayList<>();
        while (scanner.hasNext()) {
            // lecture de la positon initiale de la tondeuse
            parser.setLawnMower(scanner.nextLine());

            if (scanner.hasNext()) {
                parser.setInstructions(scanner.nextLine());
                listePositions.add(parseEtLaunchProcess(parser));
            } else {
                throw new LawnMowerException(LogMessagesConstants.ERREUR_DONNEES_INCORRECTES);
            }
        }
        return listePositions;
    }

    /**
     * Parser et executer le traitement de la tondeuse
     * @param parser : l'objet contenant les informations de la tondeuse
     * @throws LawnMowerException
     */
    private static String parseEtLaunchProcess(LawnMowerParser parser)
            throws LawnMowerException {
        if (!parser.executeParse()) {
            throw new LawnMowerException(LogMessagesConstants.ERREUR_DONNEES_INCORRECTES);
        } else {
            LawnMowerProcessService process = LawnMowerProcessServiceImpl.builder()
                    .lawn(LineProcess.formatLineLawn(parser.getLawn()))
                    .position(LineProcess.formatLineLawnMower(parser.getLawnMower()))
                    .instructionsList(LineProcess.formatLineInstruction(parser.getInstructions()))
                    .build();

            process.launchInstructions();

            log.info(process.toString());
            return process.toString();
        }
    }

     /**
     * Lecture d'un fichier donné dans resources
     * @param path : path vers le fichier en resources
     * @return file: File
     */
    public static File readFileFromResources(String path) {
       return Path.of(path).toFile();
    }
}
