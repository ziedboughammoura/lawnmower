package fr.kata.test.utils;


import fr.kata.test.entities.Coordinates;
import fr.kata.test.entities.Lawn;
import fr.kata.test.entities.LawnMowerPosition;
import fr.kata.test.enums.Instruction;
import fr.kata.test.enums.Orientation;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de traitements d'une ligne
 *
 * @author Zied Boughammoura
 */

@AllArgsConstructor
@Data
public class LineProcess {

    private static final String SPACE_TAG = " ";


    /**
     * récupere la position de la tondeuse qui est définit par ses coordonnées et son orientation
     * @param line : ligne de la position de la tondeuse ( ex : 2 3 G)
     * @return l'objet qui définit la position dela tondeuse
     */
    public static LawnMowerPosition formatLineLawnMower(String line){
        String[] elts = line.split(SPACE_TAG);
        Coordinates pCoordinates = new Coordinates(Integer.valueOf(elts[0]), Integer.valueOf(elts[1]));
        Orientation orientation = getOrientation(elts[2].charAt(0));
        return new LawnMowerPosition(pCoordinates, orientation);
    }

    /**
     * récupere l'objet Pelouse contenant les coordonnées limites de la pelouse
     * @param line : ligne de la pelouse ( ex : 2 3)
     * @return l'objet qui définit la limite de la pelouse
     */
    public static Lawn formatLineLawn(String line){
        String[] elts = line.split(SPACE_TAG);
        return new Lawn(new Coordinates(Integer.valueOf(elts[0]), Integer.valueOf(elts[1])));
    }

    /**
     * récupere une liste d'enum InstructionTondeuse correspondante à la ligne d'instruction
     * @param line : suite d'instruction ( ex : GDAGD)
     * @return une liste d'enum InstrctionTondeuse
     */
    public static List<Instruction> formatLineInstruction(String line){
        List<Instruction> instructionList = new ArrayList<>();

        for(char instructionCode : line.toCharArray()){
            Instruction instruction = Instruction.getByInstructionCode(instructionCode);
            instructionList.add(instruction);
        }
        return instructionList;
    }

    /**
     * récupere un enum Orientation correspondant au caractère de l'orientation
     * @param cOrientation : caractère de l'orientation (E, W, N, S)
     * @return l'enum correspondant à l'orientation
     */
    public static Orientation getOrientation(char cOrientation){
        for(Orientation orientation : Orientation.values()) {
            if (orientation.orientationCodeValue == cOrientation){
                return orientation;
            }
        }
        return null;
    }

    /**
     * récupere un enum InstructionTondeuse correspondant au caractère d'instrction
     * @param cInstruction : caractère de l'instruction (A, G, D)
     * @return l'enum correspondant à l'instruction
     */
    public static Instruction getInstruction(char cInstruction){
        for(Instruction instruction : Instruction.values()) {
            if (instruction.instructionCode == cInstruction) {
                return instruction;
            }
        }
        return null;
    }
}
