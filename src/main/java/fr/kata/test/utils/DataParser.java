package fr.kata.test.utils;

import fr.kata.test.enums.Instruction;
import fr.kata.test.enums.Orientation;

/**
 * Classe de traitements des données de fichier
 *
 * @author Zied Boughammoura
 */
public class DataParser {

    /**
     * Parse la position de la tondeuse et son orientation
     * La position et l'orientation sont fournies sous la forme de 2 chiffres et une lettre,
     * séparés par un espace
     * @param tondeuse
     * @return true si la ligne des positions est correcte, false sinon
     */
    public static boolean parseLawnMower(String tondeuse){
        StringBuilder stringBuilder = new StringBuilder("");
        stringBuilder.append(Orientation.NORTH.orientationCodeValue)
                .append("|").append(Orientation.SOUTH.orientationCodeValue)
                .append("|").append(Orientation.EAST.orientationCodeValue)
                .append("|").append(Orientation.WEST.orientationCodeValue);
        return tondeuse.matches("(\\d+) (\\d+) (" + stringBuilder +")");
    }

    /**
     * Parse la ligne des instructions
     * les instructions sont une suite de caractères(G, D, A) sans espaces
     * @param instructions
     * @return true si la ligne des instructions est correcte, false sinon
     */
    public static boolean parseInstructionsList(String instructions){
        StringBuilder stringBuilder = new StringBuilder("");
        stringBuilder.append("(").append(Instruction.MOVE_FORWARD.instructionCode)
                .append("|").append(Instruction.RIGHT.instructionCode)
                .append("|").append(Instruction.LEFT.instructionCode)
                .append(")+");

        return instructions.matches(stringBuilder.toString());
    }

    /**
     * Parse la position de la pelouse
     * la position de la pelouse est sous forme de 2 chiffres séparés par espace
     * @param lawn
     * @return true si la ligne des instructions est correcte, false sinon
     */
    public static boolean parseLawn(String lawn){
        return lawn.matches("(\\d+) (\\d+)");
    }
}
