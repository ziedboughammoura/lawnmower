package fr.kata.test.entities;

import fr.kata.test.enums.Orientation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Entité position de la tendeuse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class LawnMowerPosition {

    private Coordinates lawnMowerCoordinates;
    private Orientation lawnMowerOrientation;
}
