package fr.kata.test.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Entité Coordonnées
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Coordinates {

    private int x;
    private int y;

    /**
     * Vérifie si les coordonnées de la tondeuse après mouvement sont en dehors de celles de la pelouse
     *
     * @param pCoordinates : coordonnées de pelouse
     *
     * @return true si les coordonnées de la tondeuse sont à l'intérieur de la pelouse
     */
    public boolean isOutMaxCoordinates(Coordinates pCoordinates){
        return pCoordinates.getX() >= 0
                && pCoordinates.getY() >= 0
                && pCoordinates.getX() <= this.x
                && pCoordinates.getY() <= this.y;
    }

}
