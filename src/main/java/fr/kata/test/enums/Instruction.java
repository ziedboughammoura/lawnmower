package fr.kata.test.enums;


import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum Instruction {

    RIGHT('D', "Pivoter à droite"),
    LEFT('G', "Pivoter à gauche"),
    MOVE_FORWARD('A', "Avancer");

    public char instructionCode;

    public String instructionLabel;

    public char asChar;

    Instruction(char instructionCode, String instructionLabel) {
        this.instructionLabel = instructionLabel;
        this.instructionCode = instructionCode;
    }

    public static Instruction getByInstructionCode(char instructionCode) {
        for(Instruction c : values()) {
            if(c.instructionCode == instructionCode) return c;
        }
        return null;
    }
}
