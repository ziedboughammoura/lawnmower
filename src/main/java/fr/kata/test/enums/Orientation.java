package fr.kata.test.enums;

public enum Orientation {

    NORTH('N', "Nord"),
    EAST('E', "EST"),
    WEST('W', "OUEST"),
    SOUTH('S', "SUD");

    public final char orientationCodeValue;
    public final String orientationLabelValue;

    private Orientation(char codeValue, String labelValue) {
        this.orientationCodeValue = codeValue;
        this.orientationLabelValue = labelValue;
    }


}
