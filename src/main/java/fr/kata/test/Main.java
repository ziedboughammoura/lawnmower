package fr.kata.test;

import fr.kata.test.constants.LogMessagesConstants;
import fr.kata.test.exceptions.LawnMowerException;
import fr.kata.test.utils.FileUtils;
import lombok.extern.slf4j.Slf4j;


import java.io.File;
import java.io.IOException;

@Slf4j
public class Main {
    public static void main(String[] args) throws IOException, LawnMowerException {

        File file = FileUtils.readFileFromResources("src/main/resources/test_ok.txt");
        log.info(LogMessagesConstants.INIT_LAWN_MOWER_POSITION);
        FileUtils.launchLawnMowerProcess(file);
    }
}