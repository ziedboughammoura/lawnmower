package fr.kata.test.constants;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;


/**
 * Constantes des messages utilisés pour les logs
 *
 * @author Zied Boughammoura
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LogMessagesConstants {

    public static final String PREFIX_INIT  = "BO :: INIT :: " ;
    public static final String PREFIX_ERROR  = "BO :: ERROR :: " ;
    public static final String INIT_LAWN_MOWER_POSITION  = PREFIX_INIT + "Position finale des tondeuses:";

    public static final String ERREUR_DONNEES_INCORRECTES  = PREFIX_ERROR + "données incorrectes";
    public static final String ERREUR_FICHIER_INEXISTANT  = PREFIX_ERROR + "fichier inexistant";
    public static final String ERREUR_POSITION_INCORRECTE = PREFIX_ERROR + "position incorrecte";
    public static final String ERREUR_ORIENTATION_INCORRECTE = PREFIX_ERROR + "orientation incorrecte";
    public static final String ERREUR_INSTRUCTION_INCORRECTE = PREFIX_ERROR + "instruction incorrecte";


}
